package problems

/**
  * A unival tree (which stands for "universal value") is a tree where all nodes under it have the same value.
  *
  * Given the root to a binary tree, count the number of unival subtrees.
  *
  * For example, the following tree has 5 unival subtrees:
  *
  *     0
  *    / \
  *   1   0
  *      / \
  *     1   0
  *    / \
  *   1   1
  */
object CountUnivalTree {

  def countUnivalTrees(root: Node): Int = {

    def helper(root: Option[Node]): (Int, Boolean) = {
      if(root.isEmpty) {
        return (0, true)
      }

      val left = helper(root.get.left)
      val right = helper(root.get.right)
      val totalCount = left._1 + right._1

      if(left._2 && right._2) {
        if(root.get.left.nonEmpty && root.get.value != root.get.left.get.value) {
          return (totalCount, false)
        }
        if(root.get.right.nonEmpty && root.get.value != root.get.right.get.value) {
          return (totalCount, false)
        }
        return (totalCount + 1, true)
      }
      (totalCount, false)
    }

    helper(Some(root))._1

  }

}
