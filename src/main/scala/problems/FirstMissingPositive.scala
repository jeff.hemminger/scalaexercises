package problems

/*
Given an array of integers, find the first missing positive integer in linear time and constant space. In other words, find the lowest positive integer that does not exist in the array. The array can contain duplicates and negative numbers as well.

For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.

You can modify the input array in-place.
 */

object FirstMissingPositive {

  def findMissingPositive(numbers: Seq[Int]): Int = numbers.isEmpty match {
    case true => 0
    case false => sortFind(numbers)
  }

  def sortFind(numbers: Seq[Int]): Int = {
    val s = numbers.filter(i => i > -1).sorted
    var i = 0
     while (s.size > i && i == s(i)) {
       i = i + 1
     }
    i
  }

}
