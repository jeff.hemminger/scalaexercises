package problems

import scala.math.{ max => maxOfTwo }

/**
  * Given a list of integers, write a function that returns the largest sum of non-adjacent numbers. Numbers can be 0 or negative.
  *
  * For example, [2, 4, 6, 2, 5] should return 13, since we pick 2, 6, and 5. [5, 1, 1, 5] should return 10, since we pick 5 and 5.
  *
  * Follow-up: Can you do this in O(N) time and constant space?
  */
object MaxNonAdjacent {

  def maxNonAdjacent(i: Array[Int]): Int = {
    if(i.length <= 2) {
      return maxOfTwo(0, max(i))
    }
    var max_excluding_last= maxOfTwo(0, i(0))
    var max_including_last = maxOfTwo(max_excluding_last, i(1))



    i.drop(2).foreach { j =>
      val prev_max_including_last = max_including_last

      max_including_last = maxOfTwo(max_including_last, max_excluding_last + j)
      max_excluding_last = prev_max_including_last
    }
    maxOfTwo(max_including_last, max_excluding_last)
  }

  def max(a: Array[Int]) : Int = a.fold(a(0)){ (i, j) => maxOfTwo(i, j)}

}
