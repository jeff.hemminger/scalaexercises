package problems

/**
  * Given the mapping a = 1, b = 2, ... z = 26, and an encoded message, count the number of ways it can be decoded.
  *
  * For example, the message '111' would give 3, since it could be decoded as 'aaa', 'ka', and 'ak'.
  *
  * You can assume that the messages are decodable. For example, '001' is not allowed.
  */
object NumEncodings {

  def numEncodings(s: String): Int = {
    if (s.contains("0")) {
      return 0
    }

    s.sliding(2).count(i => i.toInt <= 26) + 1 // one accounts for single character encoding

  }

}
