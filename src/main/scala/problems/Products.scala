package problems

import list.List
import list.List._

/*
Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.

For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].

Follow-up: what if you can't use division?
 */
object Products {

  def div(input: List[Int]): List[Int] = map(input)( i => product3(input) / i)

  def sans_div(input: List[Int]): List[Int] = {

    def product(input: List[Int], ind: Int): Int = {
      val pre = reverse(tail(dropWhile(reverse(input))(i => i != ind)))
      val pos = tail(dropWhile(input)(i => i != ind))
      product3(append(pre, pos))
    }

    map(input) { i =>
      product(input, head(filter(input)(h => h == i)).getOrElse(0))
    }

  }

}
