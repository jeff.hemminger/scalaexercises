package problems

case class Node(value: String, left: Option[Node] = None, right: Option[Node] = None)

/*
Given the root to a binary tree, implement serialize(root), which serializes the tree into a string, and deserialize(s), which deserializes the string back into the tree.

For example, given the following Node class

class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

The following test should pass:

node = Node('root', Node('left', Node('left.left')), Node('right'))
assert deserialize(serialize(node)).left.left.val == 'left.left'

 */

object SerializeTree {

  def serialize(n: Option[Node]): String = n match {
    case None => "#"
    case Some(x) => s"${x.value} ${serialize(x.left)} ${serialize(x.right)}"
  }

  def deserialize(d: String): Option[Node] = {

    val splits = d.split(" ").iterator

    def helper(): Option[Node] = {
      val s = splits.next()
      if(s == "#") {
        None
      } else {
        Some(Node(s, helper(), helper()))
      }

    }

    helper()
  }
}