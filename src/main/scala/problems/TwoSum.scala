package problems

import list.List
import list.List._

/*
Given a list of numbers and a number k, return whether any two numbers from the list add up to k.

For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.

Bonus: Can you do this in one pass?
 */

object TwoSum {

  // This would take O(N2).
  def two_sum_brute(numbers: List[Int], sum: Int): Boolean = {
    length(filter(pairs(numbers))(a => a._1 + a._2 == sum)) != 1
  }

  // This would take O(N log N) with O(1) space.
  def two_sum_set(numbers: List[Int], sum: Int): Boolean = {
    def contains(i: List[Int], s: Set[Int]): Boolean = {
      if (length(i) == 0) {
        false
      } else if (s.contains(sum - head(i).getOrElse(0))) {
        true
      } else {
        contains(tail(i), s + head(i).getOrElse(0))
      }
    }
    contains(numbers, Set.empty)
  }

}
