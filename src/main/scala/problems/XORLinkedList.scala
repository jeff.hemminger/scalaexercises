package problems

/**
  * An XOR linked list is a more memory efficient doubly linked list. Instead of each node holding next and prev fields, it holds a field named both, which is an XOR of the next node and the previous node. Implement an XOR linked list; it has an add(element) which adds the element to the end, and a get(index) which returns the node at index.
  *
  * If using a language that has no pointers (such as Python), you can assume you have access to get_pointer and dereference_pointer functions that converts between nodes and memory addresses.
  */
object XORLinkedList {

  case class Node(v: String) {

    var b: (Option[Node], Option[Node]) = (None, None)

    def both: (Option[Node], Option[Node]) = b

    def next(n: Node) = b = (b._1, Some(n))

    def prev(n: Node) = b = (Some(n), b._2)
  }

  class XORLinkedList(var head: Node) {

    def get(i: Int): Option[Node] = {
      def findNode(target: Int, l: Node, count: Int = 0): Option[Node] = {
        if(target == count) {
          Some(l)
        } else if(l.both._2.isEmpty) {
          None
        } else {
          findNode(target, l.both._2.get, count + 1)
        }
      }

      findNode(i, head)

    }

    def add(n: Node) = {
      n.next(head)
      head.prev(n)
      head = n
    }
  }
}