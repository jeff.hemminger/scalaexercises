package problems

import org.scalatest.{FlatSpec, Matchers}

class ConsSpec  extends FlatSpec with Matchers {

  "Cons" should "return 3 when calling car" in {
    val c = new problems.Cons[Int]()

    c.car(c.cons(3, 6)) shouldBe 3
  }

  "Cons" should "return 3 when calling cdr" in {
    val c = new problems.Cons[Int]()

    c.cdr(c.cons(6,3)) shouldBe 3
  }

}
