package problems

import org.scalatest.{FlatSpec, Matchers}
import problems.CountUnivalTree.countUnivalTrees

class CountUnivalTreeSpec extends FlatSpec with Matchers  {

  "Problem Example" should "find 5" in {
    val one = Node("1", None, None)
    val zero = Node("0", Some(Node("1", Some(one), Some(one))), Some(Node("0", None, None)))
    val root = Node("0", Some(one), Some(zero))
    countUnivalTrees(root) shouldBe 5
  }

  /**
    *    a
    *   / \
    *  a   a
    *     / \
    *    a  a
    *        \
    *         A
    */
  "a example" should "find 3" in {
    val one = Node("A", None, None)
    val ia = Node("a", Some(Node("a", None, None)), Some(Node("a", None, Some(one))))
    val root = Node("a", Some(Node("a", None, None)), Some(ia))
    countUnivalTrees(root) shouldBe 3
  }

  /**
    *     a
    *    / \
    *   c   b
    *      / \
    *     b  b
    *         \
    *          b
    */
  "b example" should "find 3" in {
    val bs = Node("b", None, None)
    val cs = Node("c", None, None)
    val ia = Node("b", Some(bs), Some(Node("b", None, Some(bs))))
    val root = Node("a", Some(cs), Some(ia))
    countUnivalTrees(root) shouldBe 5
  }
}
