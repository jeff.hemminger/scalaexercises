package problems

import org.scalatest._
import problems.FirstMissingPositive.findMissingPositive

class FirstMissingPositiveSpec extends FlatSpec with Matchers {

  "FirstMissingPositive" should "find 3" in {
    findMissingPositive(Seq(-1, 0, 2, 1)) shouldBe 3
  }

  "FirstMissingPositive" should "find 1" in {
    findMissingPositive(Seq(-1, 0, 3, 2)) shouldBe 1
  }

}
