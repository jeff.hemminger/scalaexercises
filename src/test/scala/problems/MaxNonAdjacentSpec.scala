package problems

import org.scalatest.{FlatSpec, Matchers}
import problems.MaxNonAdjacent._

class MaxNonAdjacentSpec extends FlatSpec with Matchers  {

  "Max func" should "find 4" in {
    max(Array(1,2,1,1,4,3,2,1,2)) shouldBe 4

  }

  "[2, 4, 6, 2, 5]" should "return 13" in {
    maxNonAdjacent(Array(2,4,6,2,5)) shouldBe 13
  }

  "[5, 1, 1, 5]" should "return 10" in {
    maxNonAdjacent(Array(5, 1, 1, 5)) shouldBe 10
  }
}
