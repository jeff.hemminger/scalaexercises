package problems

import org.scalatest.{FlatSpec, Matchers}
import problems.NumEncodings.numEncodings

class NumEncodingSpec extends FlatSpec with Matchers {

  "111" should "yield 3" in {
    numEncodings("111") shouldBe 3
  }

  "011" should "yield 0" in {
    numEncodings("011") shouldBe 0
  }

  "602" should "yield 0" in {
    numEncodings("602") shouldBe 0
  }

  "1111" should "yield 4" in {
    numEncodings("1111") shouldBe 4
  }

  "1919" should "yield 3" in {
    numEncodings("1919") shouldBe 3
  }
}
