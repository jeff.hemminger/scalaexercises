package problems

import org.scalatest._
import Products._
import list.List

class ProductsSpec extends FlatSpec with Matchers {

  "Given [1, 2, 3, 4, 5], the expected output" should " be [120, 60, 40, 30, 24]" in {
    sans_div(List(1, 2, 3, 4, 5)) shouldBe List(120, 60, 40, 30, 24)
  }

  "Given [3, 2, 1], the expected output" should " be [2, 3, 6]" in {
    sans_div(List(3, 2, 1)) shouldBe List(2, 3, 6)
  }

  "Given [3, 2, 1], the expected output" should " be [2, 3, 6] using division" in {
    div(List(3, 2, 1)) shouldBe List(2, 3, 6)
  }
}
