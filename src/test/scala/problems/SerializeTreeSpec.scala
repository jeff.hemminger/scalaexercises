package problems

import org.scalatest._
import problems.SerializeTree._

class SerializeTreeSpec extends FlatSpec with Matchers {

  "Node" should "serialize and deserialize to itself" in {
    val node = Node("root", Some(Node("left", Some(Node("left.left")))), Some(Node("right")))
    deserialize(serialize(Some(node))) shouldBe Some(node)
  }

}