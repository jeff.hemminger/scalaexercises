package problems

import org.scalatest._
import TwoSum._
import list.List

class TwoSumSpec extends FlatSpec with Matchers {

  "Given [10, 15, 3, 7] and k of 17" should "return true since 10 + 7 is 17" in {
    two_sum_brute(List(10, 15, 3, 7), 17) shouldBe true
  }


  "Given [10, 15, 3, 7] and k of 17" should "return true since 10 + 7 is 17 by using set" in {
    two_sum_set(List(10, 15, 3, 7), 17) shouldBe true
  }

}
