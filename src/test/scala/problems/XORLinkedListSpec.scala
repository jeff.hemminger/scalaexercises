package problems

import org.scalatest.{FlatSpec, Matchers}
import problems.XORLinkedList.{ XORLinkedList, Node => NN }

class XORLinkedListSpec extends FlatSpec with Matchers  {

  "get 1" should "find node a" in {
    val xor = new XORLinkedList(NN("a"))
    xor.add(NN("b"))
    xor.get(1) shouldBe Some(NN("a"))
  }

  "get 3" should "find node d" in {
    val xor = new XORLinkedList(NN("a"))
    List("b", "c", "d").foreach( n => xor.add(NN(n)))
    xor.get(0) shouldBe Some(NN("d"))
  }


}
